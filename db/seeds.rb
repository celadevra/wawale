# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name:  "James T Kirk",
             email: "enterprise@fleet.mil",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

9.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

10.times do |n|
  name = Faker::Company.name
  province = Faker::Address.state
  city = Faker::Address.city
  nearby = Faker::Company.name
  location = Faker::Address.street_address
  phone = Faker::PhoneNumber.phone_number
  opentime = "10:00-22:00"
  cat_list = ['正餐','点心']
  Place.create!(name: name,
                province: province,
                city: city,
                nearby: nearby,
                location: location,
                phone: phone,
                opentime: opentime,
                cat_list: cat_list,
                introducer_id: rand(1..10))
end

100.times do |n|
  title = Faker::Lorem.sentence
  contents = Faker::Lorem.paragraph
  rating = rand(1..5)
  Review.create!(title: title,
                 rating: rating,
                 contents: contents,
                 user_id: rand(1..10),
                 place_id: rand(1..10))
end
