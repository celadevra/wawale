class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.integer :user_id
      t.integer :place_id
      t.foreign_key :users
      t.foreign_key :places
      t.integer :rating
      t.string :title
      t.string :contents
      t.timestamps null: false
    end
  end
end
