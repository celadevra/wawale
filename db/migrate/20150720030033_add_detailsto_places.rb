class AddDetailstoPlaces < ActiveRecord::Migration
  def change
    add_column :places, :phone, :string
    add_column :places, :opentime, :string
  end
end
