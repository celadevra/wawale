class AddAttributesToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :province, :string
    add_column :places, :community, :string
    add_column :places, :nearby, :string
  end
end
