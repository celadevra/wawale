class AddIntroduceAssociationBetweenUserAndPlaces < ActiveRecord::Migration
  def change
      add_column :places, :introducer_id, :integer, :references => "users"
      add_foreign_key :places, :users, column: :introducer_id
  end
end
