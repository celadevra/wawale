class AddIndicesToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.index :name
      t.index :email
      t.index :wechat
    end
  end
end
