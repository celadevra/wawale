## README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...


Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.

### Purpose

This is a prototype web app to verify the concept of a location-based, Wechat-enabled venue/service review and recommendation service for parents of young children (age 0-3).

### Dependencies

* Rails 4.2
* Ruby 2.1
* PostgreSQL 9.x

### Installation

Setup the Rails and Ruby environment (rbenv is recommended).

Run `bundle install`.
