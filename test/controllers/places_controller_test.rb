require 'test_helper'

class PlacesControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
    @place = places(:one)
    @place.cat_list.add('正餐')
    @place.save
    @review1 = reviews(:one)
    @review2 = reviews(:two)
    @review1.place_id = @place.id
    @review1.save
    @review2.place_id = @place.id
    @review2.save
    @review1.user_id = @review2.user_id = @user.id
    @review1.save
    @review2.save
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:places)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create place" do
    log_in_as(@user)
    assert_difference('Place.count') do
      post :create, place: { province: @place.province, city: @place.city, location: @place.location, name: @place.name, cat_list: ['正餐'], perk_list: "" }
    end

    assert_redirected_to place_path(assigns(:place))
  end

  test "should show place" do
    get :show, id: @place
    assert_response :success
  end

  test "should show place average rating" do
    get :show, id: @place
    assert_select "p#test-rating", { count:1, text: "评分\n  "+((@review1.rating+@review2.rating) / 2.0).round(2).to_s }
  end

  test "should show place list by category" do
    get :index_by_cat, cat: @place.cat_list.first
    assert_response :success
    get :show, id: Place.tagged_with('正餐').first.id
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @place
    assert_response :success
  end

  test "should update place" do
    log_in_as(@user)
    patch :update, id: @place, place: { perk_list: "", location: @place.location, name: @place.name }
    assert_redirected_to place_path(assigns(:place))
  end

  test "should destroy place" do
    assert_difference('Place.count', -1) do
      delete :destroy, id: @place
    end

    assert_redirected_to places_path
  end
end
