require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  setup do
    @users = User.all
    @count = User.count
  end

  test "should get the root page" do
    get :index
    assert_response :success
  end

  test "should show recent users" do
    get :index
    if @count < 5
      assert_select 'span a', @count
    else
      assert_select 'span a', 5
    end
  end
end
