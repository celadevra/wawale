require 'test_helper'

class ReviewsControllerTest < ActionController::TestCase
  setup do
    @review = reviews(:one)
    @place = places(:one)
    @user = users(:one)
    @review.place_id = @place.id
    @review.user_id = @user.id
    @review.save
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create review" do
    log_in_as(@user)
    assert_difference('Review.count') do
      post :create, review: { title: @review.title, contents: @review.contents, rating: @review.rating, place_id: @place.id }
    end

    assert_redirected_to place_path(Place.friendly.find(@place.id))
  end

  test "should show review" do
    get :show, id: @review
    assert_response :success
    assert_select "a", { text: @user.name, count: 1 }
    assert_select "a", { text: @place.name, count: 1 }
  end

  test "should get edit" do
    log_in_as(@user)
    get :edit, id: @review
    assert_response :success
  end

  test "should update review" do
    log_in_as(@user)
    patch :update, id: @review, review: { title: @review.title, contents: @review.contents, rating: @review.rating, place_id: @place.id }
    assert_redirected_to review_path(assigns(:review))
  end

  test "should destroy review" do
    log_in_as(@user)
    assert_difference('Review.count', -1) do
      delete :destroy, id: @review
    end

    assert_redirected_to reviews_path
  end
end
