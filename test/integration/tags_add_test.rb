require 'test_helper'
require 'uri'
class TagsAddTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:one)
    @place = places(:one)
  end
  # TODO: not really testing presence of the tags and links

  test "newly added cat tags should appear on place show page" do
    log_in_as(@user)
    patch place_path(@place.id), place: { name: @place.name, location: @place.location, community: @place.community, city: @place.city, province: @place.province, cat_list: ["", "正餐","早午餐"], perk_list: "" }
    assert_redirected_to place_path(Place.friendly.find(@place.id))
  end

  test "newly added perk tags should appear on place show page" do
    log_in_as(@user)
    patch place_path(@place.id), place: { name: @place.name, location: @place.location, community: @place.community, city: @place.city, province: @place.province, cat_list: ["", "正餐","早午餐"], perk_list: "电梯，儿童菜单" }
    assert_redirected_to place_path(Place.friendly.find(@place.id))
    get user_path(User.friendly.find(@user.id))
    assert :success
  end
end
