class UsersAdminTest < ActionDispatch::IntegrationTest

  def setup
    @admin = users(:one)
    @non_admin = users(:tcase1)
  end

  test "non admin user cannot see user list" do
    log_in_as(@non_admin)
    get root_path
    assert_select "a[href=?]", users_path, 0
  end

  test "admin user can see user list" do
    log_in_as(@admin)
    get root_path
    assert_select "a[href=?]", users_path, 1
  end

  test "logged in user can see link to place list" do
    log_in_as(@non_admin)
    get root_path
    assert_select "nav a[href=?]", places_path, 1
  end

  test "not-logged in user can't see link to place list" do
    get root_path
    assert_select "nav a[href=?]", places_path, 0
  end
end
