require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "should reject invalid inputs" do
    get new_user_path
    assert_no_difference 'User.count' do
      post users_path, user: { name:  "",
                               email: "user@invalid",
                               password:              "foo",
                               password_confirmation: "bar" }
    end
    assert_template 'users/new'
  end

  test "valid signup information" do
    get new_user_path
    assert_difference 'User.count', 1 do
      post_via_redirect users_path, user: { name:  "Example User",
                                            email: "user@example.com",
                                            password:              "password",
                                            password_confirmation: "password" }
    end
    assert_template 'users/show'
    assert is_logged_in?
  end

  test "slugs from chinese characters are pinyin" do
    get new_user_path
    post_via_redirect users_path, user: { name: "张三",
                                          email: "zhangsan@example.com",
                                          password: "password",
                                          password_confirmation: "password" }
    assert_match /zhang-san/, user_path
  end

  test "users with the same name will have a number for unique friendly id" do
    100.times do |i|
      get new_user_path
      post_via_redirect users_path, user: { name: "李四",
                                            email: "lisi#{i.to_s}@example.com",
                                            password: "password",
                                            password_confirmation: "password" }
    end
    assert User.last.friendly_id, "li-si-99"
  end

end
