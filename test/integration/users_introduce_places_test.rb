require 'test_helper'

class UsersIntroducePlacesTest < ActionDispatch::IntegrationTest

def setup
  @user = users(:one)
end

test "created place should have user's id" do
  log_in_as(@user)
  @place = @user.introduced_places.build({ name: 'Foo Bar', province: 'Beijing', city: 'Beijing', location: 'Bao Street 43', cat_list: ['正餐'] })
  @place.save
  assert @place.introducer_id == @user.id
  assert @user.introduced_places.first.id == @place.id
  get place_path(@place.id)
  assert_template 'places/show'
  assert_select "p a[href=?]", user_path(@user), count: 1
end

test "users not logged in cannot create" do
  post places_path, place: { name: 'Foo Bar', province: 'Beijing', city: 'Beijing', location: 'Bao Street 43' }
  assert_redirected_to login_path
end
end
