require 'test_helper'

class ReviewSubmitTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:tcase1)
    @place = places(:one)
  end

  test "only registered user can review" do
    get place_path(Place.friendly.find(@place.id))
    assert_select "a[href=?]", edit_place_path(@place), count: 0
    assert_select "input[id=review_title]", count: 0
    log_in_as(@user)
    get place_path(Place.friendly.find(@place.id))
    assert_template 'places/show'
    assert_select "a[href=?]", edit_place_path(@place), count: 1
    assert_select "input[id=review_title]", count: 1
  end

  test "review should be on current place" do
    log_in_as(@user)
    get place_path(Place.friendly.find(@place.id))
    post reviews_path, review: { title: "Review Title", contents: "Contents must be longer than 20 characters", rating: 3, place_id: @place.id }
    assert_redirected_to place_path(Place.friendly.find(@place.id))
  end
end
