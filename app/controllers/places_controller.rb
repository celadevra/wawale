class PlacesController < ApplicationController
  before_action :logged_in_user, only: [:create, :update]
  before_action :set_place, only: [:show, :edit, :update, :destroy]

  helper PlacesHelper

  # GET /places
  # GET /places.json
  def index
    @places = Place.all
  end

  # GET /places/1
  # GET /places/1.json
  def show
  end

  def index_by_cat
    @places_by_cat = Place.tagged_with(params[:cat]).uniq
  end

  def index_by_perk
    @places_by_perk = Place.tagged_with(params[:perk]).uniq
  end

  # GET /places/new
  def new
    @place = Place.new
  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  # POST /places.json
  def create
    @place = current_user.introduced_places.build(place_params)
    current_user.tag(@place, with: params[:place][:perk_list].split(/,|，/), on: :perk)

    respond_to do |format|
      if @place.save
        format.html { redirect_to @place, notice: 'Place was successfully created.' }
        format.json { render :show, status: :created, location: @place }
      else
        format.html { render :new }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    current_user.tag(@place, with: params[:place][:perk_list].split(/,|，/) - (@place.all_perk_list & params[:place][:perk_list].split(/,|，/)), on: :perk)
    respond_to do |format|
      if @place.update(place_params)
        format.html { redirect_to @place, notice: 'Place was successfully updated.' }
        format.json { render :show, status: :ok, location: @place }
      else
        format.html { render :edit }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    @place.destroy
    respond_to do |format|
      format.html { redirect_to places_url, notice: 'Place was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.friendly.find(params[:id])
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def place_params
      params.require(:place).permit(:name, :province, :city, :community, :nearby, :location, :phone, :opentime, :cat_list, :perk_list, :cat_list => [], :perk_list => [])
    end
end
