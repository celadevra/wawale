json.array!(@users) do |user|
  json.extract! user, :id, :name, :wechat, :phone, :email
  json.url user_url(user, format: :json)
end
