module PlacesHelper
  include ActsAsTaggableOn::TagsHelper

  def perks_count_by_usage
    perks_count = Hash.new
    User.find_each do |u|
      u.owned_taggings.group_by { |t| t.tag_id }.each do |key, value|
        perks_count.has_key?(key) ?
          perks_count[key] += value.size :
          perks_count[key] = value.size
      end
    end
    return perks_count
  end

  # => e.g. { perk1: 13, perk2: 7, perk3:... }
end
