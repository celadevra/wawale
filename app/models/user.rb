class User < ActiveRecord::Base
  extend FriendlyId
  friendly_id :processed_name_candidates, use: :slugged

  acts_as_tagger
  ActsAsTaggableOn.delimiter = [',','，']

  def processed_name
      Pinyin.t(name)
  end

  def processed_name_candidates
    [
      :processed_name,
      [:processed_name, (User.where(name: name).count - 1).to_s]
    ]
  end

  attr_accessor :remember_token
  has_secure_password
  before_save { self.email = email.downcase }
  validates :name, presence: true, length: { maximum: 30 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  has_many :introduced_places, class_name: "Place", foreign_key: "introducer_id", inverse_of: :introducer
  has_many :reviews
  has_many :reviewed_places, class_name: "Place", foreign_key: "reviewers_id", through: :reviews, source: :place

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
      BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end
end
