class Review < ActiveRecord::Base
  #extend FriendlyId
  #friendly_id :processed_name_candidates, use: :slugged
  belongs_to :user
  belongs_to :place

  validates :rating, presence: true
  validates :contents, length: { minimum: 20 }, allow_nil: true
end
