class Place < ActiveRecord::Base
  extend FriendlyId
  friendly_id :processed_name_candidates, use: :slugged

  acts_as_taggable
  acts_as_taggable_on :cat, :perk, :exp
  ActsAsTaggableOn.delimiter = [',','，']

  def processed(str)
    Pinyin.t(str)
  end

  def processed_city
    processed(city)
  end

  def processed_province
    processed(province)
  end

  def processed_name
    processed(name)
  end

  def processed_community
    community.nil? ? "" : processed(community)
  end

  def processed_name_candidates
    [
      "#{processed_province}-#{processed_city}-#{processed_name}",
      [:processed_province, :processed_city, :processed_community, :processed_name],
      [:processed_province, :processed_city, :processed_community, :processed_name, (Place.where(name: name).count - 1).to_s]
    ]
  end

  validates :name, presence: true, length: { maximum: 30 }
  validates :province, presence: true
  validates :city, presence: true
  validates :cat_list, presence: true

  belongs_to :introducer, class_name: "User", foreign_key: "introduced_places_id", inverse_of: :introduced_places
  has_many :reviews, dependent: :destroy
  has_many :reviewers, class_name: "User", foreign_key: "reviewed_places_id", through: :reviews, source: :user

  def place_cats
    [
      ['餐饮',
       ['正餐', '点心', '早午餐', '下午茶', '零食', '饮料']],
      ['游乐',
       ['游乐场', '公园', '运动场馆', '展览', '郊野']],
      ['服务',
       ['理发', '摄影', '托管', '卫生医疗']],
      ['购物',
       ['食品', '用品', '玩具', '书籍']]
    ]
  end
end
